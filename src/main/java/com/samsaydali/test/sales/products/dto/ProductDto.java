package com.samsaydali.test.sales.products.dto;

import com.samsaydali.test.sales.products.entities.Product;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class ProductDto {

    @NotNull(message = "name is required!")
    @NotBlank(message = "name is required!")
    private String name;

    private String description;

    @NotNull(message = "category is required!")
    @NotBlank(message = "category is required!")
    private String category;

    public Product toProduct() {
        Product p = new Product();
        p.setName(this.name);
        p.setCategory(this.category);
        p.setDescription(this.description);
        p.setCreationDate(new Date());
        return p;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }
}
