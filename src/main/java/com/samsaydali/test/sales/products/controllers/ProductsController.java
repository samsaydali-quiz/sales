package com.samsaydali.test.sales.products.controllers;

import com.samsaydali.test.sales.clients.dto.ClientDto;
import com.samsaydali.test.sales.products.dto.ProductDto;
import com.samsaydali.test.sales.products.entities.Product;
import com.samsaydali.test.sales.products.services.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "products")
public class ProductsController {
    private final ProductsService productsService;

    @Autowired
    public ProductsController(ProductsService service) {
        this.productsService = service;
    }

    @GetMapping
    public List<Product> getAll() {
        return this.productsService.getAll();
    }

    @PostMapping
    public Product add(@RequestBody @Valid ProductDto dto) {
        return this.productsService.create(dto);
    }

    @PutMapping("/{id}")
    public Product update(@PathVariable Long id, @RequestBody @Valid ProductDto dto) {
        return this.productsService.update(id, dto);
    }
}
