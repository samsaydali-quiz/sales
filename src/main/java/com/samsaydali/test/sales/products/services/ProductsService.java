package com.samsaydali.test.sales.products.services;

import com.samsaydali.test.sales.products.dto.ProductDto;
import com.samsaydali.test.sales.products.entities.Product;
import com.samsaydali.test.sales.products.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductsService {

    private final ProductsRepository productsRepository;

    @Autowired
    public ProductsService(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    public List<Product> getAll() {
        return this.productsRepository.findAll();
    }

    public Product create(ProductDto dto) {
        Product newProduct = dto.toProduct();
        return this.productsRepository.save(newProduct);
    }

    public Product update(Long id, ProductDto dto) {
        Product currentProduct = this.productsRepository.findById(id).orElseThrow();
        currentProduct.setDescription(dto.getDescription());
        currentProduct.setName(dto.getName());
        currentProduct.setCategory(dto.getCategory());
        return productsRepository.save(currentProduct);
    }
}
