package com.samsaydali.test.sales.sales.services;

import com.samsaydali.test.sales.aspects.LogUpdate;
import com.samsaydali.test.sales.clients.repositories.ClientsRepository;
import com.samsaydali.test.sales.products.repositories.ProductsRepository;
import com.samsaydali.test.sales.sales.dto.CreateSaleDto;
import com.samsaydali.test.sales.sales.dto.TransactionDto;
import com.samsaydali.test.sales.sales.dto.UpdateTransactionDto;
import com.samsaydali.test.sales.sales.entities.Sale;
import com.samsaydali.test.sales.sales.entities.Transaction;
import com.samsaydali.test.sales.sales.repositories.SalesRepository;
import com.samsaydali.test.sales.sales.repositories.TransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
public class SalesServices {

    private final SalesRepository salesRepository;
    private final TransactionsRepository transactionsRepository;
    private final ProductsRepository productsRepository;
    private final ClientsRepository clientsRepository;

    @Autowired
    public SalesServices(SalesRepository salesRepository,
                         TransactionsRepository transactionsRepository,
                         ProductsRepository productsRepository, ClientsRepository clientsRepository) {
        this.salesRepository = salesRepository;
        this.transactionsRepository = transactionsRepository;
        this.productsRepository = productsRepository;
        this.clientsRepository = clientsRepository;
    }

    public List<Sale> getAllSales() {
        return this.salesRepository.findAll();
    }

    @Transactional
    public Sale createSale(CreateSaleDto createSaleDto) {
        List<Transaction> transactions = getTransactionsFromSalesDto(createSaleDto);
        Sale sale = new Sale();
        sale.setCreationDate(new Date());
        sale.setClient(this.clientsRepository.findById(createSaleDto.getClientId()).orElseThrow());
        sale.setSeller(createSaleDto.getSeller());
        sale.setTotal(calcTotal(transactions));
        sale.setTransactions(transactions);
        sale =  this.salesRepository.save(sale);
        associateTransactions(sale, transactions);
        return sale;
    }

    private void associateTransactions(Sale sale, List<Transaction> transactions) {
        for (Transaction t: transactions) {
            t.setSale(sale);
            transactionsRepository.save(t);
        }
    }

    @LogUpdate
    public Transaction updateTransaction(Long id, UpdateTransactionDto updateTransactionDto) {
        Transaction t = transactionsRepository.findById(id).orElseThrow();
        int oldQuantity = t.getQuantity();
        double oldPrice = t.getPrice();
        int newQuantity = updateTransactionDto.getQuantity();
        double newPrice = updateTransactionDto.getPrice();
        t.setQuantity(newQuantity);
        t.setPrice(newPrice);
        changeSaleTotal(t.getSale(), oldPrice, oldQuantity, newPrice, newQuantity);
        return transactionsRepository.save(t);
    }

    private void changeSaleTotal(Sale sale, double oldPrice, int oldQuantity, double newPrice, int newQuantity) {
        double oldTotal = sale.getTotal();
        double newTotal = oldTotal - oldPrice * oldQuantity + newPrice * newQuantity;
        sale.setTotal(newTotal);
        salesRepository.save(sale);
    }

    private List<Transaction> getTransactionsFromSalesDto(CreateSaleDto createSaleDto) {
        List<Transaction> transactions = new LinkedList<>();
        for (TransactionDto t: createSaleDto.getTransactions()) {
            Transaction transaction = new Transaction();
            transaction.setPrice(t.getPrice());
            transaction.setProduct(this.productsRepository.findById(t.getProductId()).orElseThrow());
            transaction.setQuantity(t.getQuantity());
            transactions.add(this.transactionsRepository.save(transaction));
        }
        return transactions;
    }

    private double calcTotal(List<Transaction> transactions) {
        double total = 0;
        for (Transaction t: transactions) {
            total += t.getQuantity() * t.getPrice();
        }
        return total;
    }
}
