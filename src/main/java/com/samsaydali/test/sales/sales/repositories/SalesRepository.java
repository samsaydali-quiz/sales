package com.samsaydali.test.sales.sales.repositories;

import com.samsaydali.test.sales.sales.entities.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesRepository extends JpaRepository<Sale, Long> {
}
