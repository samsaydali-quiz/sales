package com.samsaydali.test.sales.sales.dto;

import javax.validation.constraints.NotNull;

public class UpdateTransactionDto {

    @NotNull(message = "quantity is required")
    private int quantity;

    @NotNull(message = "quantity is price")
    private double price;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
