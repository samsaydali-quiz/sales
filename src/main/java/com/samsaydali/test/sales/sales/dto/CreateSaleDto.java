package com.samsaydali.test.sales.sales.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CreateSaleDto {

    @NotNull(message = "client is required")
    private long clientId;

    @NotNull(message = "seller is required")
    @NotBlank(message = "seller is required")
    private String seller;

    private List<TransactionDto> transactions;

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public List<TransactionDto> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionDto> transactions) {
        this.transactions = transactions;
    }
}
