package com.samsaydali.test.sales.sales.repositories;

import com.samsaydali.test.sales.sales.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionsRepository extends JpaRepository<Transaction, Long> {
}
