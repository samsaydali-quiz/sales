package com.samsaydali.test.sales.sales.dto;

import javax.validation.constraints.NotNull;

public class TransactionDto {

    @NotNull(message = "product is required")
    private long productId;

    @NotNull(message = "quantity is required")
    private int quantity;

    @NotNull(message = "quantity is price")
    private double price;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
