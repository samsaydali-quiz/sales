package com.samsaydali.test.sales.sales.controllers;

import com.samsaydali.test.sales.sales.dto.CreateSaleDto;
import com.samsaydali.test.sales.sales.dto.UpdateTransactionDto;
import com.samsaydali.test.sales.sales.entities.Sale;
import com.samsaydali.test.sales.sales.entities.Transaction;
import com.samsaydali.test.sales.sales.services.SalesServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("sales")
public class SalesController {

    private final SalesServices salesServices;

    @Autowired
    public SalesController(SalesServices salesServices) {
        this.salesServices = salesServices;
    }

    @GetMapping
    public List<Sale> getAllSales() {
        return this.salesServices.getAllSales();
    }

    @PostMapping
    public Sale createSale(@RequestBody @Valid CreateSaleDto dto) {
        return this.salesServices.createSale(dto);
    }

    @PutMapping("/{id}")
    public Transaction updateTransaction(@PathVariable Long id , @RequestBody @Valid UpdateTransactionDto dto) {
        return this.salesServices.updateTransaction(id, dto);
    }
}
