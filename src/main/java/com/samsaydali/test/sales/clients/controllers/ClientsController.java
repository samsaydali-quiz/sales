package com.samsaydali.test.sales.clients.controllers;

import com.samsaydali.test.sales.clients.dto.ClientDto;
import com.samsaydali.test.sales.clients.entities.Client;
import com.samsaydali.test.sales.clients.services.ClientsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "clients")
public class ClientsController {

    private final ClientsService clientsService;

    @Autowired
    public ClientsController(ClientsService service) {
        this.clientsService = service;
    }

    @GetMapping
    public List<Client> getAll() {
        return this.clientsService.getAll();
    }

    @PostMapping
    public Client add(@RequestBody @Valid ClientDto dto) {
        return this.clientsService.create(dto);
    }

    @PutMapping("/{id}")
    public Client update(@PathVariable Long id, @RequestBody @Valid ClientDto dto) {
        return this.clientsService.update(id, dto);
    }
}
