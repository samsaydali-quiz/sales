package com.samsaydali.test.sales.clients.dto;

import com.samsaydali.test.sales.clients.entities.Client;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ClientDto {

    @NotNull(message = "name is required!")
    @NotBlank(message = "name is required!")
    private String name;

    @NotNull(message = "last name is required!")
    @NotBlank(message = "last name is required!")
    private String lastName;

    @NotNull(message = "mobile is required!")
    @NotBlank(message = "mobile is required!")
    private String mobile;

    public Client toClient() {
        Client c = new Client();
        c.setName(this.name);
        c.setLastName(this.lastName);
        c.setMobile(this.mobile);
        return c;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
