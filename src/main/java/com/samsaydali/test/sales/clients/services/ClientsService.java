package com.samsaydali.test.sales.clients.services;
import com.samsaydali.test.sales.clients.dto.ClientDto;
import com.samsaydali.test.sales.clients.entities.Client;
import com.samsaydali.test.sales.clients.repositories.ClientsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ClientsService {

    private final ClientsRepository clientsRepository;

    @Autowired
    public ClientsService(ClientsRepository clientsRepository) {
        this.clientsRepository = clientsRepository;
    }

    public List<Client> getAll() {
        return this.clientsRepository.findAll();
    }

    public Client create(ClientDto dto) {
        Client newClient = dto.toClient();
        return this.clientsRepository.save(newClient);
    }

    public Client update(Long id, ClientDto dto) {
        Client currentClient = this.clientsRepository.findById(id).orElseThrow();
        currentClient.setName(dto.getName());
        currentClient.setLastName(dto.getLastName());
        currentClient.setMobile(dto.getMobile());
        return clientsRepository.save(currentClient);
    }
}
