package com.samsaydali.test.sales.aspects;

import com.samsaydali.test.sales.sales.dto.UpdateTransactionDto;
import com.samsaydali.test.sales.sales.entities.Transaction;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class LogUpdateAspect {
    private Logger logger = LoggerFactory.getLogger(Transaction.class);

    @Before("@annotation(com.samsaydali.test.sales.aspects.LogUpdate)")
    public void logUpdate(JoinPoint joinPoint) {
        long id = (Long) joinPoint.getArgs()[0];
        double newPrice = ((UpdateTransactionDto) joinPoint.getArgs()[1]).getPrice();
        double newQuantity = ((UpdateTransactionDto) joinPoint.getArgs()[1]).getQuantity();
        logger.info("Updated transaction {}, quantity: {}, price: {}", id , newQuantity , newPrice);
    }
}
